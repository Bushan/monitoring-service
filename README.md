# Monitoring Service

Running from CLI
 
	$ java -jar monitoring-service-0.0.1.jar

or, Running main method class 'MonitoringServiceApplication.java' from IDE or CLI

It will start up on port '7979'

## Hystrix Dashboard

	http://${host}:7979/hystrix
	

## Hystrix

	http://${host}:8096/hystrix.stream

To monitor circuit breaker enabled service 

	http://${service host}:${service port}/hystrix.stream


## Docker
This is configured to build docker image and push to private nexus repository using maven plugin.
	
	mvn clean -X package docker:build -DpushImage

Maven properties

	docker.image.prefix=<nexus host>:18079
	docker.registry.id=<Docker Repository name>
	registry.url=<registry url e.g. https://192.168.1.98:8449/repository/myDockerRepo/ >